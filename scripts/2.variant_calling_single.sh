#!/bin/bash

source ../configs/pipeline.cfg
source ../configs/settings.cfg

aligner_id=$1
sw_id=$2
bam=$3
ref_id=$4

if [ $# -gt 4 ];then
	bam_type=$5
else
	bam_type="recal"
fi

prefix=${sample_id}_${aligner_id}_${sw_id}_${ref_id}

if [ ! -d  ${tmp_dir}/${prefix} ];then
	mkdir ${tmp_dir}/${prefix}
fi

if [ ! -d ${work_dir}/${prefix} ];then
	mkdir ${work_dir}/${prefix}
	mkdir ${work_dir}/${prefix}/log
fi

dbsnpidx="dbsnp_${ref_id}"
LOG_FILE="${work_dir}/${prefix}/log/${prefix}.log"

echo "[${prefix}][$(date "+%F %T")] +++ Start processing sample ${sample_id} with ${sw_id}" >> ${LOG_FILE}

if [ "$sw_id" = "gatk2_ug" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	java -Xmx${maxmem} -XX:ParallelGCThreads=${threads} -Djava.io.tmpdir=${tmp_dir}/${prefix} -jar ${gatk2}/GenomeAnalysisTK.jar -R ${b37_fasta} -T UnifiedGenotyper -glm BOTH --num_threads ${threads} -I ${bam} --dbsnp ${!dbsnpidx} -o ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf -stand_call_conf 50.0 -stand_emit_conf 10.0 -dcov 200
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "gatk3_hc" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	java -Xmx${maxmem} -XX:ParallelGCThreads=${threads} -Djava.io.tmpdir=${tmp_dir}/${prefix} -jar ${gatk3}/GenomeAnalysisTK.jar -R ${b37_fasta} -T HaplotypeCaller -I ${bam} --dbsnp ${!dbsnpidx} -o ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "gatk3_ug" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	java -Xmx${maxmem} -XX:ParallelGCThreads=${threads} -Djava.io.tmpdir=${tmp_dir}/${prefix} -jar ${gatk3}/GenomeAnalysisTK.jar -R ${b37_fasta} -T UnifiedGenotyper -glm BOTH --num_threads ${threads} -I ${bam} --dbsnp ${!dbsnpidx} -o ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "freebayes" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	${freebayes}/freebayes -f ${b37_fasta} ${bam} > ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "atlas_snp" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	ruby ${atlas_snp}/Atlas-SNP2.rb -i ${bam} -r ${b37_fasta} -o ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf -n ${sample_id} --Illumina
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "atlas_ind" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	ruby ${atlas_indel}/Atlas-Indel2.rb -b ${bam} -r ${b37_fasta} -o ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf -I -s ${sample_id}
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "glfsingle" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED piling up bam file for ${prefix}" >> ${LOG_FILE}
	${samtools_hybrid}/samtools-hybrid pileup -g -f ${b37_fasta} ${bam} > ${work_dir}/${prefix}/${prefix}_${bam_type}.glf
	echo "[${prefix}][$(date "+%F %T")] DONE piling up bam file for ${prefix}" >> ${LOG_FILE}
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	${glfsingle}/glfSingle -g ${work_dir}/${prefix}/${prefix}_${bam_type}.glf -b ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "ivc" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	cd ${work_dir}/${prefix}
	cp ${ivc}/etc/ivc_config_default.ini ${work_dir}/${prefix}/config.ini
	${ivc}/bin/configureWorkflow.pl --bam ${bam} --ref=${b37_fasta} --config=${work_dir}/${prefix}/config.ini --output=${work_dir}/${prefix}/${sample_id}_${bam_type}
	cd ${work_dir}/${prefix}/${sample_id}_${bam_type} && make -j ${threads}
	ln -s ${work_dir}/${prefix}/${sample_id}_${bam_type}/results/* ${work_dir}/${prefix}
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "samtools" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	${samtools}/samtools mpileup -ugf ${b37_fasta} ${bam} | ${bcftools}/bcftools call -vmO v -o ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "varscan" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	${samtools}/samtools mpileup -f ${b37_fasta} ${bam} | java -Xmx10g -XX:ParallelGCThreads=${threads} -Djava.io.tmpdir=${tmp_dir} -jar ${varscan}/VarScan.v2.3.7.jar mpileup2cns --output-vcf 1 --variants > ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
elif [ "$sw_id" = "platypus" ];then
	echo "[${prefix}][$(date "+%F %T")] STARTED calling variants with ${prefix}" >> ${LOG_FILE}
	python ${platypus}/Platypus.py callVariants --refFile ${b37_fasta} --bamFiles ${bam} --output ${work_dir}/${prefix}/${prefix}_${bam_type}.vcf
	echo "[${prefix}][$(date "+%F %T")] DONE calling variants with ${prefix}" >> ${LOG_FILE}
fi

echo "[${prefix}][$(date "+%F %T")] +++ End processing sample ${sample_id} with ${sw_id}" >> ${LOG_FILE}
